#!/usr/bin/env iocsh.bash

# Required modules
## module_name,version


require raritanpdu
# Environment variables
#
## Database macros
#epicsEnvSet(P, "LabS-Utgard-VIP:Rack-Chopper")
#epicsEnvSet(R, "PDU")
#epicsEnvSet(TOP, "$(E3_CMD_TOP)")
epicsEnvSet("SNMP_CMD_TOP", "$(raritanpdu_DIR)")
epicsEnvSet(LOCATION, "Utgard: 172.30.244.111")
epicsEnvSet(IPADDR,   "172.30.244.111")
epicsEnvSet(IOCNAME,  "utg-raritanpdu-001")
epicsEnvSet(MIB, "/home/iocuser/.conda/envs/$(IOCNAME)")

##Load IOCSH
iocshLoad("$(raritanpdu_DIR)raritan-px3-5190r.iocsh" "PDU_IP=$(IPADDR),IOC=$(IOCNAME),MIB_HOME=$(MIB)"


iocshLoad("$(essioc_DIR)/essioc.iocsh")


iocInit()

#dbl > "$(TOP)/$(IOC)_PVs.list"
