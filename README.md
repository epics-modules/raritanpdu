# raritanpdu

European Spallation Source ERIC Site-specific EPICS module: raritanpdu

Additonal information:
* [Documentation](https://confluence.esss.lu.se/display/IS/POCs+-+Raritan+PDU+PX3-5190R+and+PX3-5260R)
* [Release notes](RELEASE.md)
* [Requirements](https://gitlab.esss.lu.se/nice/staging-recipes/raritanpdu-recipe/-/blob/master/recipe/meta.yaml#L18)
* [Building and Testing](https://confluence.esss.lu.se/display/IS/1.+Development+locally+with+e3+and+Conda#id-1.Developmentlocallywithe3andConda-BuildanEPICSmoduleandtestitlocally)
